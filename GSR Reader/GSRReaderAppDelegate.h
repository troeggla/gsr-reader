//
//  GSRReaderAppDelegate.h
//  GSR Reader
//
//  Created by Thomas Röggla on 15.05.2014.
//  Copyright (c) 2014 Thomas Röggla. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "IntegerFormatter.h"

#import "ORSSerialPort.h"
#import "ORSSerialPortManager.h"
#import "SRWebSocket.h"

@interface GSRReaderAppDelegate : NSObject <NSApplicationDelegate, ORSSerialPortDelegate, SRWebSocketDelegate> {
@private
    NSMutableArray* sensorIds;
    NSNumber* selectedSensorIndex;
    
    IBOutlet NSArrayController* arrayController;
}

@property (assign) IBOutlet NSWindow* window;

@property (weak) IBOutlet NSPopUpButton* serialPortComboBox;
@property (weak) IBOutlet NSTextField* field_value;
@property (weak) IBOutlet NSTextField* sensor_id_label;
@property (weak) IBOutlet NSLevelIndicator* level_indicator;
@property (weak) IBOutlet NSButton *startStopButton;
@property (weak) IBOutlet NSTextField *websocketHostname;
@property (weak) IBOutlet NSTextField *websocketPort;
@property (weak) IBOutlet NSButton *websocketCheckbox;
@property (weak) IBOutlet NSPopUpButton *sensorSelector;
@property (weak) IBOutlet NSButton *fileCheckbox;
@property (weak) IBOutlet NSTextField *filenameLabel;

@property NSMutableArray* sensorIds;
@property (nonatomic, copy) NSNumber* selectedSensorIndex;

@property ORSSerialPort* serialPort;
@property NSMutableData* incomingDataBuffer;
@property NSData* delimiter;
@property SRWebSocket* webSocket;
@property NSFileHandle* savePathHandle;

@property BOOL dataCollectionEnabled;
@property BOOL webSocketOpened;
@property BOOL saveToFile;

- (IBAction)websocketCheckboxChecked:(id)sender;
- (IBAction)startStopReading:(NSButton *)sender;
- (IBAction)saveFileCheckboxChecked:(id)sender;

- (void)startDataCollection;
- (void)stopDataCollection;

- (void)startWebSocketConnection:(NSString*)hostname;
- (void)stopWebSocketConnection;
- (NSData*)createWebsocketMessage:(NSInteger)value withId:(NSInteger)sensorId;

- (void)processDataPacket:(NSString*)packet;
- (NSArray*)getValidPackets;

- (void)showAlert:(NSString*)message withTitle:(NSString*)title;

- (BOOL)sensorIdPresent:(NSInteger)sensorId;

@end
