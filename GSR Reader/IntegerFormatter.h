//
//  IntegerFormatter.h
//  GSR Reader
//
//  Created by Thomas Röggla on 31.08.2015.
//  Copyright (c) 2015 Thomas Röggla. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IntegerFormatter : NSNumberFormatter

@end
