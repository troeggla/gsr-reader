//
//  IntegerFormatter.m
//  GSR Reader
//
//  Created by Thomas Röggla on 31.08.2015.
//  Copyright (c) 2015 Thomas Röggla. All rights reserved.
//

#import "IntegerFormatter.h"

@implementation IntegerFormatter

- (BOOL)isPartialStringValid:(NSString*)partialString newEditingString:(NSString**)newString errorDescription:(NSString**)error {
    if([partialString length] == 0) {
        return TRUE;
    }
    
    NSScanner* scanner = [NSScanner scannerWithString:partialString];
    
    if(!([scanner scanInt:0] && [scanner isAtEnd])) {
        NSBeep();
        return FALSE;
    }
    
    return TRUE;
}

@end
