//
//  main.m
//  GSR Reader
//
//  Created by Thomas Röggla on 15.05.2014.
//  Copyright (c) 2014 Thomas Röggla. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
