//
//  GSRReaderAppDelegate.m
//  GSR Reader
//
//  Created by Thomas Röggla on 15.05.2014.
//  Copyright (c) 2014 Thomas Röggla. All rights reserved.
//

#import "GSRReaderAppDelegate.h"

@implementation GSRReaderAppDelegate

@synthesize sensorIds;
@synthesize selectedSensorIndex;

- (void)showAlert:(NSString*)message withTitle:(NSString*)title {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:title];
    [alert setInformativeText:message];
    [alert runModal];
}

- (void)startDataCollection {
    [self.serialPortComboBox setEnabled:false];
    [self.level_indicator setEnabled:true];
    [self.startStopButton setTitle:@"Stop"];
    
    NSString *portName = [[self.serialPortComboBox selectedItem] title];
    
    [self.serialPort close];
    self.serialPort = [ORSSerialPort serialPortWithPath:portName];
    
    self.serialPort.baudRate = [NSNumber numberWithInt:115200];
    self.serialPort.delegate = self;
    
    [self.serialPort open];
    self.dataCollectionEnabled = true;
    
    [self.fileCheckbox setEnabled:FALSE];
}

- (void)stopDataCollection {
    NSLog(@"Stopping data collection");
    [self.serialPort close];
    
    [self.serialPortComboBox setEnabled:true];
    
    [self.level_indicator setEnabled:false];
    [self.level_indicator setIntValue:0];
    
    [self.field_value setStringValue:@""];
    [self.startStopButton setTitle:@"Start"];
    self.dataCollectionEnabled = false;
    
    [self.savePathHandle closeFile];
    self.saveToFile = false;
    [self.fileCheckbox setState:0];
    [self.fileCheckbox setEnabled:TRUE];
    [self.filenameLabel setStringValue:@""];
}

- (void)startWebSocketConnection:(NSString*)hostname {
    [self.webSocket close];
    self.webSocket = [[SRWebSocket alloc] initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:hostname]]];
    self.webSocket.delegate = self;
    [self.webSocket open];
    
    [self.websocketHostname setEnabled:FALSE];
    [self.websocketPort setEnabled:FALSE];
}

- (void)stopWebSocketConnection {
    self.webSocketOpened = false;
    [self.webSocket close];
    self.webSocket.delegate = nil;
    
    [self.websocketHostname setEnabled:TRUE];
    [self.websocketPort setEnabled:TRUE];
    
    [self.websocketCheckbox setState:0];
}

- (NSArray*)getValidPackets {
    NSMutableArray* packets = [[NSMutableArray alloc] init];
    
    while (true) {
        NSRange range = [self.incomingDataBuffer
                            rangeOfData:self.delimiter
                            options:0
                            range:NSMakeRange(0, self.incomingDataBuffer.length)];
        
        if (range.location != NSNotFound) {
            size_t body_offset = NSMaxRange(range);
            NSData* packetBuffer = [self.incomingDataBuffer subdataWithRange:NSMakeRange(0, body_offset - 1)];
            
            NSString* packet = [[[[NSString alloc] initWithData:packetBuffer encoding:NSUTF8StringEncoding]
                                 stringByReplacingOccurrencesOfString:@" " withString:@""]
                                 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            
            if (packet != nil) {
                [packets addObject:packet];
            }

            [self.incomingDataBuffer
             replaceBytesInRange:NSMakeRange(0, body_offset)
             withBytes:NULL
             length:0];
        } else {
            break;
        }
    }
    
    return packets;
}

- (void)processDataPacket:(NSString*)packet {
    NSArray* components = [packet componentsSeparatedByString:@","];
    
    if ([components count] == 8) {
        NSInteger val = [components[3] integerValue];
        NSInteger sensorId = [components[2] integerValue];
        
        if ([components[1] isEqualToString:@""] && val >= 0 && val <= 1023) {
            if (![self sensorIdPresent:sensorId]) {
                NSLog(@"Adding sensor ID: %ld", sensorId);
                NSDictionary *item = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:sensorId], @"name", nil];
                [arrayController addObject:item];
                
                if ([sensorIds count] == 1) {
                    [self.sensorSelector selectItemAtIndex:0];
                    selectedSensorIndex = [NSNumber numberWithInteger:sensorId];
                }
            }
            
            if ([selectedSensorIndex integerValue] == sensorId) {
                [self.sensor_id_label setIntegerValue:sensorId];
                
                [self.field_value setIntegerValue:val];
                [self.level_indicator setIntegerValue:val];
            }
            
            if (self.saveToFile) {
                NSString *row = [NSString stringWithFormat:@"%f,%ld,%ld\n", [[NSDate date] timeIntervalSince1970], sensorId, val];
                [self.savePathHandle writeData:[row dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            if (self.webSocket != nil && self.webSocketOpened) {
                NSData* jsonData = [self createWebsocketMessage:val withId:sensorId];
                [self.webSocket send:jsonData];
            }
        }
    }
}

- (NSData*)createWebsocketMessage:(NSInteger)value withId:(NSInteger)sensorId {
    NSDictionary* data = @{
        @"type": @"data",
        @"channel": @"gsr",
        @"data": @{
            @"timestamp": [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],
            @"id": @1,
            @"value": [NSNumber numberWithLong:value]
        }
    };
    
    return [NSJSONSerialization dataWithJSONObject:data options:0 error:nil];
}

- (BOOL)sensorIdPresent:(NSInteger)sensorId {
    for (NSDictionary* item in sensorIds) {
        if ([item[@"name"] integerValue] == sensorId) {
            return TRUE;
        }
    }
    
    return FALSE;
}

#pragma mark - NSApplicationDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    sensorIds = [[NSMutableArray alloc] init];
    [arrayController setContent:sensorIds];
    
    ORSSerialPortManager* portManager = [ORSSerialPortManager sharedSerialPortManager];
    NSLog(@"%@", [[portManager availablePorts] description]);
    
    [portManager addObserver:self forKeyPath:@"availablePorts" options:NSKeyValueObservingOptionNew context:NULL];
    [self observeValueForKeyPath:@"availablePorts" ofObject:portManager change:nil context:nil];
    
    self.level_indicator.minValue = 0;
    self.level_indicator.maxValue = 1023;
    
    self.delimiter = [NSData dataWithBytes:(unsigned char[]){0x0a} length:1];
    self.incomingDataBuffer = [[NSMutableData alloc] init];
    
    self.dataCollectionEnabled = false;
    self.webSocketOpened = false;
    self.saveToFile = false;
    
    [self.websocketPort setStringValue:@"23149"];
    [self.websocketPort setFormatter:[[IntegerFormatter alloc] init]];
    
    NSLog(@"Alright, let's get started here...");
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    NSLog(@"Terminating...");
    
    [self.serialPort close];
    [self.webSocket close];
    [self.savePathHandle closeFile];
}

#pragma mark - NSKeyValueObserving

- (void) observeValueForKeyPath:keypath ofObject:obj change:change context:(void*)ctx {
    if ([keypath isEqualToString:@"availablePorts"]) {
        ORSSerialPortManager *portManager = (ORSSerialPortManager*)obj;
        [self.serialPortComboBox removeAllItems];
        NSLog(@"%@", change);
        
        for (ORSSerialPort* port in [portManager availablePorts]) {
            if ([[port path] containsString:@"usbserial"]) {
                [self.serialPortComboBox addItemWithTitle:[port path]];
            }
            
            NSLog(@"%@", [port path]);
        }
        
        if ([self.serialPortComboBox numberOfItems] == 0) {
            [self.serialPortComboBox setEnabled:false];
            [self.startStopButton setEnabled:false];
        } else {
            [self.serialPortComboBox setEnabled:true];
            [self.startStopButton setEnabled:true];
        }
        
        NSLog(@"TRIGGERED CALLBACK");
    }
}

#pragma mark - IBAction

- (IBAction)websocketCheckboxChecked:(id)sender {
    NSButton* checkbox = (NSButton*)sender;
    
    if ([checkbox state] == 1) {
        NSLog(@"Checkbox checked");
        NSString* hostname = [self.websocketHostname stringValue];
        NSInteger portnum = [self.websocketPort integerValue];
        
        if ([hostname isEqualToString:@""] || portnum == 0) {
            [self showAlert:@"Please insert a valid hostname and port number" withTitle:@"Hostname or port invalid"];
            [checkbox setState:0];
            
            return;
        }
        
        hostname = [@"ws://" stringByAppendingFormat:@"%@:%ld", hostname, portnum];
        NSLog(@"Hostname: %@", hostname);
        
        [self startWebSocketConnection:hostname];
    } else {
        NSLog(@"Checkbox unchecked");
        [self stopWebSocketConnection];
    }
}

- (IBAction)startStopReading:(NSButton *)sender {
    if ([[self.startStopButton title] isEqual: @"Stop"]) {
        [self stopDataCollection];
    } else {
        [self startDataCollection];
    }
}

- (IBAction)saveFileCheckboxChecked:(id)sender {
    NSButton *checkbox = (NSButton*)sender;
    
    if ([checkbox state] == 1) {
        NSSavePanel *savePanel = [NSSavePanel savePanel];
        [savePanel setNameFieldLabel:@"Filename:"];
    
        [savePanel beginWithCompletionHandler:^(NSInteger result) {
            if (result == NSFileHandlingPanelOKButton) {
                NSURL *savePath = [savePanel URL];
            
                self.saveToFile = true;
            
                [[NSFileManager defaultManager] createFileAtPath:[savePath path] contents:nil attributes:nil];
                self.savePathHandle = [NSFileHandle fileHandleForWritingToURL:savePath error:nil];
            
                NSLog(@"save path: %@", [savePath absoluteString]);
                [self.filenameLabel setStringValue:[savePath absoluteString]];
            } else {
                [checkbox setState:0];
                self.saveToFile = false;
            }
        }];
    } else {
        [self.savePathHandle closeFile];
        [self.filenameLabel setStringValue:@""];
        
        self.saveToFile = false;
    }
}

#pragma mark - ORSSerialPortDelegate

- (void)serialPort:(ORSSerialPort *)serialPort didReceiveData:(NSData *)data {
    [self.incomingDataBuffer appendData:data];
    
    NSArray* validPackets = [self getValidPackets];
    
    for (NSString* validPacket in validPackets) {
        if (self.dataCollectionEnabled) {
            [self processDataPacket:validPacket];
        }
    }
}

- (void)serialPortWasRemovedFromSystem:(ORSSerialPort *)serialPort {
    NSLog(@"Serial port removed");
    [self stopDataCollection];
}

#pragma mark - SRWebSocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"Messge received: %@", message);
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket {
    NSLog(@"WebSocket connection established");
    self.webSocketOpened = true;
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    NSLog(@"WebSocket error: %@", error);
    
    [self showAlert:[error localizedDescription] withTitle:@"WebSocket Error"];
    [self stopWebSocketConnection];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    NSLog(@"WebSocket closed: %@", reason);
    
    [self showAlert:reason withTitle:@"WebSocket connection closed"];
    [self stopWebSocketConnection];
}

@end
